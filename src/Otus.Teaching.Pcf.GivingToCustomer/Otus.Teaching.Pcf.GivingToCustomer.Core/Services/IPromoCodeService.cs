﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public interface IPromoCodeService
    {
        Task<IEnumerable<PromoCode>> GetAllPromoCodeAsync();

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <param name="dto">ДТО промокода</param>
        /// <returns></returns>
        Task GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeDto dto);

        Task<IEnumerable<Customer>> GetCustomersWithPreferences(Guid preferenceId);
    }
}