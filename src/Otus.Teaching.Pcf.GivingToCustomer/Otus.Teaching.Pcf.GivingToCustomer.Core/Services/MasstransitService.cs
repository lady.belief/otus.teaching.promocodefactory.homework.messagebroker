﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Hosting;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class MasstransitService : IHostedService
    {
        public MasstransitService(IBusControl busControl)
        {
            _busControl = busControl;
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Start Masstransit service");
            await _busControl.StartAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _busControl.StopAsync(cancellationToken);
            Console.WriteLine("Stop Masstransit service");
        }
        
        private readonly IBusControl _busControl;
    }
}