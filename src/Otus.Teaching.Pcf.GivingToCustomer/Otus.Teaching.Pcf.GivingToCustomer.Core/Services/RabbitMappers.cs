﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public static class RabbitMappers
    {
        public static PromoCode MapFromDto_PromoCode(PromoCodeDto dto)
        {
            return new PromoCode()
            {
                Id = dto.Id,
                Code = dto.Code,
                ServiceInfo = dto.ServiceInfo,
                BeginDate = dto.BeginDate,
                EndDate = dto.EndDate,
                PartnerId = dto.PartnerId,
                PreferenceId = dto.PreferenceId
            };
        }
        
        public static PromoCodeDto MapToDto_PromoCode(PromoCode promoCode)
        {
            return new PromoCodeDto()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate,
                EndDate = promoCode.EndDate,
                PartnerId = promoCode.PartnerId,
                PreferenceId = promoCode.PreferenceId,
            };
        }
    }
}