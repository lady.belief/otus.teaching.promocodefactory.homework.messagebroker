﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Services
{
    public class PromoCodeService: IPromoCodeService
    {
        public PromoCodeService(IRepository<PromoCode> promoCodesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _customersRepository = customersRepository;
        }

        public async Task<IEnumerable<PromoCode>> GetAllPromoCodeAsync()
        {
            return await _promoCodesRepository.GetAllAsync();
        }

        public async Task GivePromoCodesToCustomersWithPreferenceAsync(PromoCodeDto dto)
        {
            var promoCode = RabbitMappers.MapFromDto_PromoCode(dto);
            await _promoCodesRepository.AddAsync(promoCode);
            
            //  Получаем клиентов с этим предпочтением:
            var customers = GetCustomersWithPreferences(promoCode.PreferenceId).Result;
            
            foreach (var customer in customers)
            {
                customer.PromoCodes ??= new List<PromoCodeCustomer>();
                if (customer.PromoCodes.Any(pc => pc.CustomerId == customer.Id && pc.PromoCodeId == promoCode.Id))
                    throw new InvalidOperationException(
                        $"The promo code = {promoCode.Id} has already been issued to the customer = {customer.Id}");
                
                customer.PromoCodes.Add(new PromoCodeCustomer()
                {
                    CustomerId = customer.Id,
                    PromoCodeId = promoCode.Id
                });
                await _customersRepository.UpdateAsync(customer);
            }
        }

        public async Task<IEnumerable<Customer>> GetCustomersWithPreferences(Guid preferenceId)
        {
            return await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preferenceId));
        }

        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customersRepository;
    }
}