﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Consumers
{
    public class PromoCodeConsumer : IConsumer<PromoCodeDto>
    {
        public PromoCodeConsumer(IPromoCodeService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }
        
        public async Task Consume(ConsumeContext<PromoCodeDto> context)
        {
            await _promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
        
        private readonly IPromoCodeService _promoCodesService;
    }
}