﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        public static PromoCode MapFromModel(ReceivingPromoCodeRequest request, Preference preference, Partner partner) {

            var promocode = new PromoCode();

            promocode.PartnerId = partner.Id;
            promocode.Partner = partner;
            promocode.Code = request.PromoCode;
            promocode.ServiceInfo = request.ServiceInfo;
           
            promocode.BeginDate = DateTime.Now;
            promocode.EndDate = DateTime.Now.AddDays(30);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.PartnerManagerId = request.PartnerManagerId;

            return promocode;
        }
    }
    
    public static class RabbitMappers
    {
        public static PromoCode MapFromDto(PromoCodeDto dto)
        {
            return new PromoCode()
            {
                Id = dto.Id,
                Code = dto.Code,
                ServiceInfo = dto.ServiceInfo,
                BeginDate = dto.BeginDate,
                EndDate = dto.EndDate,
                PartnerId = dto.PartnerId,
                PartnerManagerId = dto.PartnerManagerId,
                PreferenceId = dto.PreferenceId
            };
        }
        
        public static PromoCodeDto MapToDto(PromoCode promoCode)
        {
            return new PromoCodeDto()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate,
                EndDate = promoCode.EndDate,
                PartnerId = promoCode.PartnerId,
                PartnerManagerId = promoCode.PartnerManagerId,
                PreferenceId = promoCode.PreferenceId,
            };
        }
    }
}
