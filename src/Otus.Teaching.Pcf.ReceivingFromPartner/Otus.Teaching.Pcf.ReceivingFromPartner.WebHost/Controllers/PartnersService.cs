﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Mappers;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    public class PartnersService: IPartnersService
    {
        public PartnersService(IBusControl busControl, IRepository<Partner> partnersRepository)
        {
            _busControl = busControl;
            _partnersRepository = partnersRepository;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            await _busControl.Publish(RabbitMappers.MapToDto(promoCode));
        }
        
        private readonly IBusControl _busControl;
        private readonly IRepository<Partner> _partnersRepository;
    }
}