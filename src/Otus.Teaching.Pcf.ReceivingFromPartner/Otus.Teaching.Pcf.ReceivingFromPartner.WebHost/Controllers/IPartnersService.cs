﻿using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    public interface IPartnersService
    {
        public Task GivePromoCodeToCustomer(PromoCode promoCode);
    }
}