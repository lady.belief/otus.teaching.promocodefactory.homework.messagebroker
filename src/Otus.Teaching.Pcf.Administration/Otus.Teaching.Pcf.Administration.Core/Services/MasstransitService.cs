﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Hosting;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class MasstransitService : IHostedService
    {
        public MasstransitService(IBusControl busControl)
        {
            _busControl = busControl;
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine($"{nameof(StartAsync)}: Start Masstransit service");
            await _busControl.StartAsync(cancellationToken);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _busControl.StopAsync(cancellationToken);
            Console.WriteLine($"{nameof(StopAsync)}: Stop Masstransit service");
        }
        
        private readonly IBusControl _busControl;
    }
}