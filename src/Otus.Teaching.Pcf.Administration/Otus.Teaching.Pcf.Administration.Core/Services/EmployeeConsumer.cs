﻿using System;
using System.Threading.Tasks;
using MassTransit;

namespace Otus.Teaching.Pcf.Administration.Core.Services
{
    public class EmployeeConsumer: IConsumer<PromoCodeDto>
    {
        public EmployeeConsumer(IAdministrationService service)
        {
            _administrationService = service;
        }
        
        public async Task Consume(ConsumeContext<PromoCodeDto> context)
        {
            //context.Message.PartnerManagerId - это id employee
            if (context.Message.PartnerManagerId == null)
            {
                Console.WriteLine($"{nameof(Consume)}: employee id is null");
                return;
            }

            var id = (Guid)context.Message.PartnerManagerId;
            var employee = await _administrationService.GetByIdAsync(id);

            if (employee == null)
            {
                Console.WriteLine($"{nameof(Consume)}: employee is not found, id = {id}");
                return;
            }

            employee.AppliedPromocodesCount++;

            await _administrationService.UpdateAsync(employee);
        }

        private readonly IAdministrationService _administrationService;
    }
}